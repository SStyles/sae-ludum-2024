using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOgre : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip[] _ogreSounds = new AudioClip[5];

    float soundDelay = 2f;
    [SerializeField] float soundDistance = 12f;
    [SerializeField] Vector2 _MinMaxSoundDelay = new Vector2(2f, 5f);

    [SerializeField] GameObject _player;

    // Start is called before the first frame update
    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        soundDelay = GetRandomSoundDelayMinMax(_MinMaxSoundDelay.x, _MinMaxSoundDelay.y);

        _player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 directionVector = _player.transform.position - transform.position;
        float distance = Mathf.Lerp(1f, 0f, directionVector.magnitude/soundDistance);

        _audioSource.volume = distance;

        if (_audioSource.isPlaying)
            return;
        
        soundDelay -= Time.deltaTime;

        if(soundDelay < 0.0f)
        {
            PlayOgreSound();
            soundDelay = GetRandomSoundDelayMinMax(_MinMaxSoundDelay.x, _MinMaxSoundDelay.y);
        }
    }

    void PlayOgreSound()
    {
        _audioSource.PlayOneShot(GetRandomOgreSound());
    }

    AudioClip GetRandomOgreSound()
    {
        return _ogreSounds[Random.Range(0, _ogreSounds.Length)];
    }

    float GetRandomSoundDelayMinMax(float min, float max)
    {
        return Random.Range(min, max);
    }
}
