using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnHurtOrge : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip[] _hurtSounds = new AudioClip[3];
    private AudioClip _hurtSound;

    private Health _health;

    private void Awake()
    {
        _health = GetComponent<Health>();
        _health.OnHurt += OnHurt;
    }

    private void OnHurt(int _, GameObject _sdijf)
    {
        if (_audioSource.isPlaying)
            _audioSource.Stop();
        _hurtSound = _hurtSounds[Random.Range(0, _hurtSounds.Length)];
        _audioSource.PlayOneShot(_hurtSound);
    }
}
