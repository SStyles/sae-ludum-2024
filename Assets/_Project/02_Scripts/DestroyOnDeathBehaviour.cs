﻿using System;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class DestroyOnDeathBehaviour : MonoBehaviour
{
    [SerializeField] private float destroyTime = 0f;

    private Health _health;

    private void Awake()
    {
        _health = GetComponent<Health>();
    }

    private void OnEnable()
    {
        _health.OnDeath += OnDeath;
    }

    private void OnDisable()
    {
        _health.OnDeath -= OnDeath;
    }

    private void OnDeath()
    {
        Destroy(gameObject, destroyTime);
    }
}