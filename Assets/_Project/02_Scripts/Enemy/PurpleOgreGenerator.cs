using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurpleOgreGenerator : MonoBehaviour
{
    [SerializeField] private Sprite[] _body = new Sprite[2];
    [SerializeField] private SpriteRenderer _bodyRender;
    [SerializeField] private Sprite[] _eyes = new Sprite[4];
    [SerializeField] private SpriteRenderer _eyeRender;
    [SerializeField] private Sprite[] _hair = new Sprite[3];
    [SerializeField] private SpriteRenderer _hairRender;
    [SerializeField] private Sprite[] _head = new Sprite[2];
    [SerializeField] private SpriteRenderer _headRender;
    [SerializeField] private Sprite[] _noses = new Sprite[3];
    [SerializeField] private SpriteRenderer _noseRender;
    [SerializeField] private Sprite[] _weapon = new Sprite[2];
    [SerializeField] private SpriteRenderer _weaponRender;
    [SerializeField] private Sprite[] _leg = new Sprite[2];
    [SerializeField] private SpriteRenderer _leg1Render;
    [SerializeField] private SpriteRenderer _leg2Render;

    // Start is called before the first frame update
    void Start()
    {
        _bodyRender.sprite = _body[Random.Range(0, _body.Length)];
        _eyeRender.sprite = _eyes[Random.Range(0, _eyes.Length)];
        _hairRender.sprite = _hair[Random.Range(0, _hair.Length)];
        _headRender.sprite = _head[Random.Range(0, _head.Length)];
        _noseRender.sprite = _noses[Random.Range(0, _noses.Length)];
        _weaponRender.sprite = _weapon[Random.Range(0, _weapon.Length)];
        int legIndex = Random.Range(0, _leg.Length);
        _leg1Render.sprite = _leg[legIndex];
        _leg2Render.sprite = _leg[legIndex];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
