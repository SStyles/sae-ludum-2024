﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class StoopidEnemyBehaviour : MonoBehaviour
    {
        [FormerlySerializedAs("speed")] [SerializeField]
        private float _speed;

        [SerializeField] private int _damage = 3;

        [FormerlySerializedAs("turningPointLeft")] [FormerlySerializedAs("raycastPointLeft")] [SerializeField]
        private Transform _turningPointLeft;

        [FormerlySerializedAs("turningPointRight")] [FormerlySerializedAs("raycastPointRight")] [SerializeField]
        private Transform _turningPointRight;

        private int _movementHash;
        private bool _isGoingLeft;
        private Vector3 _scaleDirection = new(1f, 1f, 1f);
        private float _movementX;

        private Rigidbody2D _rigidbody;
        private Animator _animator;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _animator = GetComponentInChildren<Animator>();
        }

        private void Start()
        {
            _movementHash = Animator.StringToHash("Movement");
        }

        private void Update()
        {
            _scaleDirection.x = _movementX switch
            {
                > .1f => -1f,
                < -.1f => 1f,
                _ => _scaleDirection.x
            };

            gameObject.transform.localScale = _scaleDirection;

            float movementAnimationValue = _movementX is > .1f or < -.1f ? 1.0f : 0.0f;
            _animator.SetFloat(_movementHash, movementAnimationValue);
        }

        private void FixedUpdate()
        {
            var movement = new Vector3(_isGoingLeft ? -1f : 1f, 0.0f);
            _rigidbody.velocity = movement * _speed;
            _movementX = movement.x;

            var turningPoint = _isGoingLeft ? _turningPointLeft : _turningPointRight;
            float diff = (transform.position.x - turningPoint.position.x) * (_isGoingLeft ? -1f : 1f);

            if (diff > 0f)
            {
                _isGoingLeft = !_isGoingLeft;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(_turningPointLeft.position, .1f);
            Gizmos.DrawSphere(_turningPointRight.position, .1f);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.gameObject.CompareTag("Player")) return;

            if (other.gameObject.TryGetComponent(out Health health))
            {
                health.Hurt(_damage);
            }
        }
    }
}