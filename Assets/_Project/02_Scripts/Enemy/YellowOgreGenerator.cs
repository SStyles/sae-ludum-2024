using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowOgreGenerator : MonoBehaviour
{

    [SerializeField] private Sprite[] _eyes = new Sprite[2];
    [SerializeField] private SpriteRenderer _eyeRender;
    [SerializeField] private Sprite[] _head = new Sprite[2];
    [SerializeField] private SpriteRenderer _headRender;
    [SerializeField] private Sprite[] _noses = new Sprite[2];
    [SerializeField] private SpriteRenderer _noseRender;
    [SerializeField] private SpriteRenderer _earingRender;
    [SerializeField] private SpriteRenderer _braceletRender;
    

    // Start is called before the first frame update
    void Start()
    {
        _eyeRender.sprite = _eyes[Random.Range(0, _eyes.Length)];
        _headRender.sprite = _head[Random.Range(0, _head.Length)];
        _noseRender.sprite = _noses[Random.Range(0, _noses.Length)];
        int index = 0;
        index = Random.Range(0, 1);
        _earingRender.enabled = index == 1 ? true : false;
        index = Random.Range(0, 1);
        _braceletRender.enabled = index == 1 ? true : false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
