using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomGenerator : MonoBehaviour
{
    [SerializeField] private Sprite[] _mushrooms = new Sprite[6];
    [SerializeField] private SpriteRenderer _mushroomsRender;

    [SerializeField] private Vector2 _minMaxScale = new Vector2(1f, 3f);


    void Start()
    {
        _mushroomsRender.sprite = _mushrooms[Random.Range(0, _mushrooms.Length)];

        float scale = Random.Range(_minMaxScale.x, _minMaxScale.y);
        Vector3 mushroomScale = new Vector3(scale, scale, 1f);
        transform.localScale = mushroomScale;


    }
}
