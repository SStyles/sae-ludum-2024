using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteAlways]
public class TreeGenerator : MonoBehaviour
{
    [SerializeField] private Vector2 _minMaxScale = new Vector2(1f, 3f);

    [SerializeField] private Sprite[] _tree = new Sprite[2];
    [SerializeField] private SpriteRenderer _treeRender;

    [SerializeField] private Sprite[] _tree1 = new Sprite[2];
    [SerializeField] private Sprite[] _tree12 = new Sprite[2];
    [SerializeField] private SpriteRenderer _tree1Render1;
    [SerializeField] private Sprite[] _tree2 = new Sprite[2];
    [SerializeField] private Sprite[] _tree22 = new Sprite[2];
    [SerializeField] private SpriteRenderer _tree1Render2;
    [SerializeField] private Sprite[] _tree3 = new Sprite[2];
    [SerializeField] private SpriteRenderer _treeRender3;
    
    [SerializeField] private GameObject tree2AdditionnalPart;

    // Start is called before the first frame update
    void Start()
    {
        float scale = Random.Range(_minMaxScale.x, _minMaxScale.y);
        Vector3 mushroomScale = new Vector3(scale, scale, 1f);
        transform.localScale = mushroomScale;

        GenerateTree();
    }


    public void GenerateTree()
    {
        int index = Random.Range(0, _tree.Length);
        _treeRender.sprite = _tree[index];

        if (index == 0)
        {
            tree2AdditionnalPart.SetActive(false);
            _tree1Render1.sprite = _tree1[Random.Range(0, _tree1.Length)];
            _tree1Render2.sprite = _tree2[Random.Range(0, _tree2.Length)];
        }
        else
        {
            tree2AdditionnalPart.SetActive(true);
            _tree1Render1.sprite = _tree12[Random.Range(0, _tree12.Length)];
            _tree1Render2.sprite = _tree22[Random.Range(0, _tree22.Length)];
            _treeRender3.sprite = _tree3[Random.Range(0, _tree3.Length)];
        }

    }
}
