using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterMover : MonoBehaviour
{
    private MeshRenderer _renderer;

    [SerializeField] private float scrollingSpeed = 1f;

    private void Awake()
    {
        _renderer = GetComponent<MeshRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float offset = Time.time * scrollingSpeed;
        _renderer.material.SetTextureOffset("_BaseMap", new Vector2(offset, 0));
    }
}
