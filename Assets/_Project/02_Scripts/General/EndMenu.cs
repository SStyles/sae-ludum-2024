using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MyBox;

public class EndMenu : MonoBehaviour
{

    [Header("UI Transition")]
    [Tooltip("The duratrion of a FadeIn transition")]
    [SerializeField] private float _fadeInDuration = 1.0f;
    private float _maxFadeInDuration;

    [Tooltip("The image to set a Color Fade In/Out on")]
    [SerializeField] private Image _transitionImage;

    [SerializeField] public SceneReference _sceneReference;
    
    //Components
    private Color _currentColor;

    private void Start()
    {
        _maxFadeInDuration = _fadeInDuration;
        _currentColor = _transitionImage.color = Color.black;
    }

    private void Update()
    {
        FadeInTransition();
    }

    /// <summary>
    /// Fades in to a new scene
    /// </summary>
    private void FadeInTransition()
    {
        _transitionImage.color = _currentColor;

        _fadeInDuration -= Time.deltaTime;
        float currentValue = _fadeInDuration / _maxFadeInDuration;

        if (currentValue > 0.0f)
        {

            //Transition image Color Lerp
            _currentColor.a = currentValue;
            _transitionImage.color = _currentColor;
            //Music transition lerp
            //_musicAudioSource.volume = 1 - currentValue;
        }
        else
        {
            currentValue = 0.0f;
        }
    }

    public void LoadNextScene()
    {
        if (_sceneReference == null)
            return;
        _sceneReference.LoadScene();
    }
}
