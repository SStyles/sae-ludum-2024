using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MyBox;

public class EndTrigger : MonoBehaviour
{
    [SerializeField] private Image _transitionImage;


    [Header("UI Transition")]
    [Tooltip("The duratrion of a FadeIn transition")]
    [SerializeField] private float _fadeInDuration = 1.0f;
    private float _maxFadeInDuration;
    [Tooltip("The duratrion of a FadeOut transition")]
    [SerializeField] private float _fadeOutDuration = 1.0f;
    private float _maxFadeOutDuration;

    //Components
    private Color _currentColor;

    bool endScene = false;

    //Scene

    [SerializeField] private SceneReference _sceneReference;

    private void Start()
    {
        _maxFadeOutDuration = _fadeOutDuration;
        _maxFadeInDuration = _fadeInDuration;

        _currentColor = _transitionImage.color = Color.black;
    }


    private void Update()
    {
        if (endScene)
        {
            FadeOutTransition();
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Collision");
        if (collision.CompareTag("Player"))
        {
            endScene = true;
        }
    }
    private void FadeOutTransition()
    {
        //Gets the current color of the transition Image
        _transitionImage.color = _currentColor;
        //Time value
        _fadeOutDuration -= Time.deltaTime;

        if (_fadeOutDuration > 0.0f)
        {
            _fadeOutDuration -= Time.deltaTime;
            //Transition image Lerp to final color
            _currentColor.a = 1 - _fadeOutDuration;
            _transitionImage.color = _currentColor;
            //Music lerp
            //_musicAudioSource.volume = currentValue;

        }
        else
        {
            //Activate Next Scene
            _sceneReference.LoadScene();

        }
    }
}
