﻿using MyBox;
using UnityEngine;
using UnityEngine.Serialization;

namespace General
{
    public class StartMenu : MonoBehaviour
    {
        [SerializeField] private SceneReference _scene;

        public void LoadFirstLevel()
        {
            _scene.LoadScene();
        }
    }
}