﻿using System;
using MyBox;
using UnityEngine;

public class Health : MonoBehaviour
{
    public Action<int, GameObject> OnHurt;
    public Action OnDeath;

    [SerializeField] private int maxHealth;
    [SerializeField] [ReadOnly] private int currentHealth;

    public int CurrentHealth => currentHealth;
    public bool IsDead => CurrentHealth <= 0;

    private void Awake()
    {
        currentHealth = maxHealth;
    }

    public void Hurt(int damage, GameObject source = null)
    {
        currentHealth -= damage;
        OnHurt?.Invoke(damage, source);
        if (IsDead)
        {
            OnDeath?.Invoke();
        }
    }

    public void Kill(GameObject source = null)
    {
        int oldHealth = currentHealth;
        currentHealth = 0;
        OnHurt?.Invoke(oldHealth, source);
        OnDeath?.Invoke();
    }
}