using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageIndicator : MonoBehaviour
{
    [SerializeField] private Health health;


    [SerializeField] private SpriteRenderer[] spriteRenderers = new SpriteRenderer[7];

    [SerializeField] private float invicibleTime = 2f;
    [SerializeField] private float blinkFrequency = 10f;

    float time = 0;
    float blinkTime = 2f;
    bool wasHurt;

    private void Awake()
    {
        health = GetComponentInParent<Health>();

        spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        health.OnHurt += OnHurt;
        blinkTime = invicibleTime;
    }

    // Update is called once per frame
    void Update()
    {
        time = Time.time;


        if (wasHurt)
        {
            if(blinkTime > 0.0f)
            {
                BlinkEffect();
                blinkTime -= Time.deltaTime;
            }
            else
            {
                wasHurt = false;
                blinkTime = invicibleTime;
                ResetRenderersColor();
            }
        }
    }

    private void OnHurt(int _, GameObject _sdijf)
    {
        wasHurt = true;   
    }

    private void BlinkEffect()
    {
        foreach (SpriteRenderer sr in spriteRenderers)
        {
            Color spriteColor = sr.color;
            spriteColor.a = SinFloat();
            sr.color = spriteColor;
        }
    }

    private void ResetRenderersColor()
    {
        foreach (SpriteRenderer sr in spriteRenderers)
        {
            Color spriteColor = sr.color;
            spriteColor.a = 1f;
            sr.color = spriteColor;
        }
    }

    private float SinFloat()
    {
        float alphaValue;
        alphaValue = (Mathf.Sin(time * blinkFrequency) + 1) / 2;

        return alphaValue;
    }

}
