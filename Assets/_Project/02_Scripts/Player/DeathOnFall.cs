﻿using UnityEngine;

namespace Player
{
    public class DeathOnFall : MonoBehaviour
    {
        [SerializeField] private Transform _deathTreshold;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _fallSound;

        private Health _health;

        private void Awake()
        {
            _health = GetComponent<Health>();
        }

        private void FixedUpdate()
        {
            if (transform.position.y <= (_deathTreshold is null ? -10f : _deathTreshold.position.y))
            {
                _audioSource.PlayOneShot(_fallSound);
                _health.Kill();
            }
        }
    }
}