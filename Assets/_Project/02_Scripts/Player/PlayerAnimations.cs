using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    [Header("Reference Script")]
    [SerializeField] private Animator _animator;
    [SerializeField] private PlayerControler playerControler;
    [SerializeField] private PlayerMovement playerMovement;

    //Animator Hashes
    //Movement Hashes
    private int _movementHash;
    private int _isGroundedHash;
    private int _jumpHash;
    //Summoning hashes
    private int _summon0Hash;
    private int _summon1Hash;
    private int _summon2Hash;
    private int _summon3Hash;

    float movement;

    Vector3 scaleDirection = new Vector3(1f, 1f, 1f);

    void Awake()
    {
        if (_animator == null)
            _animator = GetComponentInChildren<Animator>();
        if (playerControler == null)
            playerControler = GetComponentInChildren<PlayerControler>();
        if (playerMovement == null)
            playerMovement = GetComponentInChildren<PlayerMovement>();

        SetAnimatorHashes();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateAnimatorHashes();
    }

    void SetAnimatorHashes()
    {
        _movementHash = Animator.StringToHash("Movement");
        _jumpHash = Animator.StringToHash("Jump");
        _isGroundedHash = Animator.StringToHash("isGrounded");
        _summon0Hash = Animator.StringToHash("Summon0");
        _summon1Hash = Animator.StringToHash("Summon1");
        _summon2Hash = Animator.StringToHash("Summon2");
        _summon3Hash = Animator.StringToHash("Summon3");
    }

    void UpdateAnimatorHashes()
    {
        movement = playerMovement.Movement.x;
        float movementAnimationValue = 0f;
        if (movement > .1f) 
        {
            scaleDirection.x = 1f;
        }
        if (movement < -.1f)
        {
            scaleDirection.x = -1f;
        }
        gameObject.transform.localScale = scaleDirection;

        movementAnimationValue = movement > .1f || movement < -.1f ?  1.0f : 0.0f;
        _animator.SetFloat(_movementHash, movementAnimationValue);
        
        bool isGrounded = playerMovement.IsGrounded;
        _animator.SetBool(_isGroundedHash, isGrounded);

        if (playerControler.Jump && playerMovement.IsGrounded)
            _animator.SetTrigger(_jumpHash);
    }

    private void OnSummonUp()
    {
        _animator.SetTrigger(_summon0Hash);
    }
    private void OnSummonDown()
    {
        _animator.SetTrigger(_summon1Hash);
    }
    private void OnSummonLeft()
    {
        _animator.SetTrigger(_summon2Hash);
    }
    private void OnSummonRight()
    {
        _animator.SetTrigger(_summon3Hash);
    }
}
