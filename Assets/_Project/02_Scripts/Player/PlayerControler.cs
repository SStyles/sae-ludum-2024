using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Player
{
    public class PlayerControler : MonoBehaviour
    {
        //Movement
        [FormerlySerializedAs("_movement")] public Vector2 Movement;
        [FormerlySerializedAs("_jump")] public bool Jump = false;
        public bool CanMove = true;

        //Reference Scripts
        [FormerlySerializedAs("playerInput")] [Header("Reference Components")] [SerializeField]
        private PlayerInput _playerInput;

        private void Awake()
        {
            if (_playerInput == null)
            {
                _playerInput = GetComponent<PlayerInput>();
            }
        }

        public void OnMove(InputValue value)
        {
            Movement = CanMove ? value.Get<Vector2>() : Vector2.zero;
        }

        public void OnJump(InputValue value)
        {
            Jump = value.isPressed;
        }
    }
}