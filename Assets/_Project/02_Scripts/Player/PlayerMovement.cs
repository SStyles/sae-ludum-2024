using UnityEngine;
using UnityEngine.Serialization;

namespace Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [FormerlySerializedAs("_jumpForce")] public float JumpForce = 5f;

        [Header("Reference Components")] [SerializeField]
        private PlayerControler _playerController;

        [SerializeField] private Rigidbody2D _rigidbody;
        [SerializeField] private CapsuleCollider2D _collider;

        [Header("Movement variables")] [SerializeField]
        private float _movementSpeed = 2f;

        [SerializeField] private float _gravityScale = 1f;
        [SerializeField] private float _fallGravityScale = 1f;

        [FormerlySerializedAs("_isGrounded")] public bool IsGrounded = true;
        [FormerlySerializedAs("movement")] public Vector2 Movement;

        private void Awake()
        {
            if (_playerController == null)
                _playerController = GetComponent<PlayerControler>();
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody2D>();
            if (_collider == null)
                _collider = GetComponent<CapsuleCollider2D>();
        }

        private void FixedUpdate()
        {
            Movement = new Vector2(_playerController.Movement.x, _playerController.Movement.y);
            _rigidbody.velocity = new Vector2(Movement.x * _movementSpeed, _rigidbody.velocity.y);

            if (_playerController.Jump && IsGrounded)
            {
                _rigidbody.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
                _playerController.Jump = false;
                IsGrounded = false;
            }

            _rigidbody.gravityScale = _rigidbody.velocity.y > 0 ? _gravityScale : _fallGravityScale;
        }

        //Check collisions
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Ground"))
            {
                IsGrounded = true;
            }
        }
    }
}