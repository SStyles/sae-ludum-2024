﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Player
{
    public class RespawnOnDeath : MonoBehaviour
    {
        private Health _health;

        private void Awake()
        {
            _health = GetComponent<Health>();
            _health.OnDeath += OnDeath;
        }

        private static void OnDeath()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}