﻿using System;
using UnityEngine;

namespace Player
{
    public class SoundOnHurtBehaviour : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _hurtSound;

        private Health _health;

        private void Awake()
        {
            _health = GetComponent<Health>();
            _health.OnHurt += OnHurt;
        }

        private void OnHurt(int _, GameObject _sdijf)
        {
            _audioSource.PlayOneShot(_hurtSound);
        }
    }
}