﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Player.Summon
{
    public class BadSummonBehaviour : MonoBehaviour
    {
        [SerializeField] private GameObject crotPrefab;
        [SerializeField] private Transform crotSpawnPoint;

        public void PrintBadSummon()
        {
            Debug.Log("You just did n'importe nawak");
        }

        public void SpawnCrot()
        {
            Instantiate(crotPrefab, crotSpawnPoint.position, crotSpawnPoint.rotation);
        }
    }
}