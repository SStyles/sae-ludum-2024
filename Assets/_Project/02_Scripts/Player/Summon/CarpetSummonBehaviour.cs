﻿using MyBox;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;

namespace Player.Summon
{
    [RequireComponent(typeof(PlayerControler), typeof(Health), typeof(Rigidbody2D))]
    public class CarpetSummonBehaviour : MonoBehaviour
    {
        [SerializeField] private float _travelDistance = 4f;
        [SerializeField] private float _timeToTravel = 1f;
        [SerializeField] private float _flyYOffset = 0.2f;
        [MustBeAssigned] [SerializeField] private GameObject _carpetVisuals;

        private float _moveTimer = 0f;
        private bool _isCarpeting = false;
        private Vector2 _startPos;
        private Vector2 _targetPos;

        private PlayerControler _playerControler;
        private Health _health;
        private Rigidbody2D _rigidbody;

        private void Awake()
        {
            _playerControler = GetComponent<PlayerControler>();
            _health = GetComponent<Health>();
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        private void OnEnable()
        {
            _health.OnHurt += OnHurt;
        }

        private void OnDisable()
        {
            _health.OnHurt -= OnHurt;
        }

        private void OnHurt(int _, GameObject _go)
        {
            if (_isCarpeting)
            {
                StopCarpeting();
            }
        }

        private void Update()
        {
            if (_isCarpeting)
            {
                _moveTimer += Time.deltaTime;
            }

            if (_moveTimer >= _timeToTravel)
            {
                StopCarpeting();
            }
        }

        private void FixedUpdate()
        {
            if (!_isCarpeting) return;

            float moveProgress = _moveTimer / _timeToTravel;
            var currentPos = Vector2.Lerp(_startPos, _targetPos, moveProgress);
            transform.position = currentPos;
        }

        private void StopCarpeting()
        {
            _moveTimer = 0f;
            _isCarpeting = false;
            _playerControler.CanMove = true;
            _carpetVisuals.SetActive(false);
            _rigidbody.bodyType = RigidbodyType2D.Dynamic;
        }

        public void SummonCarpet()
        {
            _playerControler.CanMove = false;
            _isCarpeting = true;
            _startPos = transform.position;
            _startPos.y += _flyYOffset;

            _targetPos = new Vector2(_startPos.x + (_travelDistance * transform.localScale.x),
                _startPos.y);
            _carpetVisuals.SetActive(true);
            _rigidbody.bodyType = RigidbodyType2D.Kinematic;
        }
    }
}