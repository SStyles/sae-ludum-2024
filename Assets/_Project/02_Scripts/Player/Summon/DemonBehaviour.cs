﻿using System;
using UnityEngine;

namespace Player.Summon
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class DemonBehaviour : MonoBehaviour
    {
        [SerializeField] private float speed = 10f;
        [SerializeField] private float timeOnEarth = 1f;
        [SerializeField] private int damage = 10;


        public float Direction
        {
            get => _direction;
            set
            {
                _direction = value;
                transform.localScale = new Vector3(_direction, 1f, 1f);
            }
        }

        private Rigidbody2D _rigidbody;
        [SerializeField] private float _direction = 1f;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            timeOnEarth -= Time.deltaTime;

            if (timeOnEarth <= 0f)
            {
                Destroy(gameObject);
            }
        }

        private void FixedUpdate()
        {
            _rigidbody.velocity = new Vector2(Direction * speed, 0f);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.gameObject.CompareTag("Enemy")) return;

            if (other.gameObject.TryGetComponent(out Health health))
            {
                health.Hurt(damage);
            }
        }
    }
}