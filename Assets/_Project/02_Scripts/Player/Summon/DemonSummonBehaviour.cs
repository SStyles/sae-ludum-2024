﻿using UnityEngine;

namespace Player.Summon
{
    public class DemonSummonBehaviour : MonoBehaviour
    {
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private GameObject demonPrefab;

        public void SpawnDemon()
        {
            var demonObject = Instantiate(demonPrefab, spawnPoint.position, spawnPoint.rotation);
            if (demonObject.TryGetComponent(out DemonBehaviour demonBehaviour))
            {
                demonBehaviour.Direction = transform.localScale.x > 0f ? 1f : -1f;
            }
        }
    }
}