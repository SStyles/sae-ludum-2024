﻿using UnityEngine;

namespace Player.Summon
{
    public class FlyAwaySummonBehaviour : MonoBehaviour
    {
        public void PrintSomethingAndDontFlyAway()
        {
            Debug.Log("You're gonna fly one day");
        }
    }
}