﻿using System;
using UnityEngine;

namespace Player.Summon
{
    public class PegaseSummonBehaviour : MonoBehaviour
    {
        [SerializeField] private float _pegaseJumpForce = 600;
        [SerializeField] private GameObject _pegasusAnimations;
        [SerializeField] private float _pegasusAnimationTime = 1f;
        [SerializeField] private PegasusAnimator pAnim;
        [SerializeField] private AudioClip _flyAudio;
        [SerializeField] private AudioSource _audioSource;


        private PlayerControler _playerControler;
        private PlayerMovement _playerMovement;

        private float _oldJumpForce;
        private int _timerResetJump = 0;
        private float _pegasusTime = 0f;

        private void Awake()
        {
            _playerControler = GetComponent<PlayerControler>();
            _playerMovement = GetComponent<PlayerMovement>();
            pAnim = _pegasusAnimations.GetComponent<PegasusAnimator>();
            _audioSource ??= GetComponent<AudioSource>();
        }

        private void Update()
        {
            if (_pegasusTime > 0f)
            {
                _pegasusTime -= Time.deltaTime;
            }
            else
            {
                _pegasusAnimations.SetActive(false);
            }
        }

        private void FixedUpdate()
        {
            _timerResetJump -= 1;
            if (_timerResetJump == 0)
            {
                _playerMovement.JumpForce = _oldJumpForce;
            }
        }

        public void SummonPegase()
        {
            if (_timerResetJump > 0 || _pegasusTime > 0f) return;
            _oldJumpForce = _playerMovement.JumpForce;
            _playerMovement.JumpForce = _pegaseJumpForce;
            _playerControler.Jump = true;
            _timerResetJump = 2;
            _pegasusTime = _pegasusAnimationTime;
            _pegasusAnimations.SetActive(true);
            pAnim.GeneratePegasus();
            _audioSource.PlayOneShot(_flyAudio);
        }
    }
}