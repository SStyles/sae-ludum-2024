using UnityEngine;

namespace Player.Summon
{
    public class PegasusAnimator : MonoBehaviour
    {
        [SerializeField] private Sprite[] _body = new Sprite[2];
        [SerializeField] private SpriteRenderer _bodyRender;
        [SerializeField] private Sprite[] _head = new Sprite[2];
        [SerializeField] private SpriteRenderer _headRender;
        [SerializeField] private Sprite[] _tail = new Sprite[2];
        [SerializeField] private SpriteRenderer _tailRender;

        [SerializeField] private Sprite[] _wings = new Sprite[3];
        [SerializeField] private SpriteRenderer _wingsRender;
        [SerializeField] private float frameSwitchFrequency = 100f;

        // Start is called before the first frame update
        void Start()
        {
            GeneratePegasus();
        }

        private void Update()
        {
            int wingIndex = (Mathf.RoundToInt(Mathf.Sin(Time.time * frameSwitchFrequency) + 1));
            _wingsRender.sprite = _wings[wingIndex];
        }

        public void GeneratePegasus()
        {
            int index = Random.Range(0, 2);
            _bodyRender.sprite = _body[index];
            _headRender.sprite = _head[index];
            _tailRender.sprite = _tail[index];
        }
    }
}