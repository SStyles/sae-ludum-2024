﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Player.Summon
{
    public class PlayerSummonBehaviour : MonoBehaviour
    {
        public Action OnUpdateSummonSentence;
        [FormerlySerializedAs("onBadSummon")] public UnityEvent OnBadSummon;

        [FormerlySerializedAs("cooldownOnFailedSummon")] [FormerlySerializedAs("timeBetweenSummons")] [SerializeField]
        private float _cooldownOnFailedSummon = 1f;

        [FormerlySerializedAs("entries")] [SerializeField]
        private SummonStringEntry[] _entries;

        [SerializeField] private AudioSource _waSound;
        [SerializeField] private AudioSource _piSound;
        [SerializeField] private AudioSource _tiSound;
        [SerializeField] private AudioSource _koSound;


        private SummonSentence _currentSentence;
        private SummonEntry[] _summonEntries;
        private bool _canSummon = true;

        public SummonSentence CurrentSentence => _currentSentence;

        private void Awake()
        {
            _currentSentence = new SummonSentence(new SummonKeyword[SummonSentence.MaxSummonKeywords]);
            FillEntries();
        }

        private void FillEntries()
        {
            _summonEntries = new SummonEntry[_entries.Length];
            for (var i = 0; i < _entries.Length; i++)
            {
                _summonEntries[i] = new SummonEntry
                {
                    SummonEvent = _entries[i].summonEvent,
                    Cooldown = _entries[i].cooldown,
                };

                var sentence = SummonSentence.FromString(_entries[i].summonString);
                if (sentence is null)
                {
                    Debug.LogError("Bad sentence");
                    return;
                }

                _summonEntries[i].SummonSentence = (SummonSentence)sentence;
            }
        }

        private void AddKeyword(SummonKeyword keyword)
        {
            if (!_canSummon) return;

            bool success = _currentSentence.PushKeyword(keyword);
            if (!success)
            {
                Debug.LogWarning("Something is fishy");
                return;
            }

            OnUpdateSummonSentence.Invoke();

            for (var i = 0; i < _summonEntries.Length; i++)
            {
                if (_summonEntries[i].SummonSentence != _currentSentence) continue;

                _summonEntries[i].SummonEvent.Invoke();
                _canSummon = false;
                StartCoroutine(RestoreSummonRights(_summonEntries[i].Cooldown));
                return;
            }

            if (_currentSentence.SummonKeywordHistoryLength < SummonSentence.MaxSummonKeywords) return;

            _canSummon = false;
            OnBadSummon.Invoke();
            StartCoroutine(RestoreSummonRights(_cooldownOnFailedSummon));
        }

        private IEnumerator RestoreSummonRights(float cooldown)
        {
            yield return new WaitForSeconds(cooldown);
            _currentSentence.Clear();
            OnUpdateSummonSentence.Invoke();
            _canSummon = true;
        }

        private void OnSummonUp()
        {
            AddKeyword(SummonKeyword.Wa);
            if (_canSummon)
                _waSound.Play();
        }

        private void OnSummonDown()
        {
            AddKeyword(SummonKeyword.Pi);
            if (_canSummon)
                _piSound.Play();
        }

        private void OnSummonLeft()
        {
            AddKeyword(SummonKeyword.Ti);
            if (_canSummon)
                _tiSound.Play();
        }

        private void OnSummonRight()
        {
            AddKeyword(SummonKeyword.Ko);
            if (_canSummon)
                _koSound.Play();
        }
    }
}