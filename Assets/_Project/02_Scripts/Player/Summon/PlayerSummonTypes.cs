﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Player.Summon
{
    public enum SummonKeyword : ushort
    {
        // Nothing
        None,

        /// Up
        Wa,

        /// Down
        Pi,

        /// Left
        Ti,

        /// Right
        Ko,
    }

    public struct SummonSentence : IEquatable<SummonSentence>
    {
        public const int MaxSummonKeywords = 6;

        private readonly SummonKeyword[] _summonKeywordHistory;
        private int _summonKeywordHistoryLength;

        public SummonSentence(SummonKeyword[] summonKeywords, int length = 0)
        {
            _summonKeywordHistory = summonKeywords;
            _summonKeywordHistoryLength = length;
        }

        public int SummonKeywordHistoryLength => _summonKeywordHistoryLength;

        public SummonKeyword[] SummonKeywordHistory => _summonKeywordHistory;

        public static SummonSentence? FromString(string sentenceString)
        {
            if (sentenceString.Length % 2 != 0)
            {
                Debug.LogError("Sentence string's length should be a multiple of 2");
                return null;
            }

            string lowercaseSentence = sentenceString.ToLower();

            int length = lowercaseSentence.Length / 2;
            var keywords = new SummonKeyword[MaxSummonKeywords];

            for (var i = 0; i < lowercaseSentence.Length; i += 2)
            {
                switch (lowercaseSentence[i])
                {
                    case 'w':
                        keywords[i / 2] = SummonKeyword.Wa;
                        break;
                    case 'p':
                        keywords[i / 2] = SummonKeyword.Pi;
                        break;
                    case 't':
                        keywords[i / 2] = SummonKeyword.Ti;
                        break;
                    case 'k':
                        keywords[i / 2] = SummonKeyword.Ko;
                        break;
                    default:
                        Debug.LogWarning("Invalid keyword in summon sentence.");
                        return null;
                }
            }

            return new SummonSentence(keywords, length);
        }

        public bool PushKeyword(SummonKeyword keyword)
        {
            if (_summonKeywordHistoryLength >= MaxSummonKeywords)
            {
                return false;
            }

            _summonKeywordHistory[_summonKeywordHistoryLength++] = keyword;
            return true;
        }

        public void Clear()
        {
            _summonKeywordHistoryLength = 0;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < _summonKeywordHistoryLength; i++)
            {
                var keyword = _summonKeywordHistory[i];
                if (keyword == SummonKeyword.None) continue;
                sb.Append(keyword);
            }

            return sb.ToString();
        }

        public bool Equals(SummonSentence other)
        {
            if (_summonKeywordHistoryLength != other._summonKeywordHistoryLength)
            {
                return false;
            }

            for (var i = 0; i < _summonKeywordHistoryLength; ++i)
            {
                if (_summonKeywordHistory[i] != other._summonKeywordHistory[i])
                {
                    return false;
                }
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            return obj is SummonSentence other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_summonKeywordHistory, _summonKeywordHistoryLength);
        }

        public static bool operator ==(SummonSentence left, SummonSentence right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(SummonSentence left, SummonSentence right)
        {
            return !(left == right);
        }
    }

    [Serializable]
    public struct SummonStringEntry
    {
        public string summonString;
        public UnityEvent summonEvent;
        public float cooldown;
    }

    internal struct SummonEntry
    {
        public SummonSentence SummonSentence;
        public UnityEvent SummonEvent;
        public float Cooldown;
    }
}