﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Player.Summon
{
    public class PlayerSummonUI : MonoBehaviour
    {
        [SerializeField] private PlayerSummonBehaviour playerSummonBehaviour;
        [SerializeField] private Image[] TextImages = new Image[SummonSentence.MaxSummonKeywords];
        [SerializeField] private Sprite[] TextSprites = new Sprite[4];

        private void Awake()
        {
            playerSummonBehaviour ??= FindAnyObjectByType<PlayerSummonBehaviour>();
        }

        private void OnEnable()
        {
            playerSummonBehaviour.OnUpdateSummonSentence += OnUpdateSummonSentence;
        }

        private void OnDisable()
        {
            playerSummonBehaviour.OnUpdateSummonSentence -= OnUpdateSummonSentence;
        }

        private void ClearImages()
        {
            foreach (var image in TextImages)
            {
                image.sprite = null;
                image.enabled = false;
            }
        }

        private void OnUpdateSummonSentence()
        {
            ClearImages();

            SummonKeyword[] sentence = playerSummonBehaviour.CurrentSentence.SummonKeywordHistory;
            int length = playerSummonBehaviour.CurrentSentence.SummonKeywordHistoryLength;

            for (int i = 0; i < length; i++)
            {
                TextImages[i].sprite = KeywordToSprite(sentence[i]);
                TextImages[i].enabled = true;
            }
        }

        Sprite KeywordToSprite(SummonKeyword keyword)
        {
            switch (keyword)
            {
                case SummonKeyword.Wa:
                    return TextSprites[0];
                case SummonKeyword.Pi:
                    return TextSprites[1];
                case SummonKeyword.Ti:
                    return TextSprites[2];
                case SummonKeyword.Ko:
                    return TextSprites[3];
                default:
                    return null;
            }
        }
    }
}